# k8s Consul Cluster

[[_TOC_]]

Epic: https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/789

## Summary

Consul is currently used within Gitlab primarily for Rails applications and PGBouncer to discover the database leader and replica nodes via Consul Service DNS resolution. It's also used by Patroni for service healthchecks and as a backend for storing the Patroni cluster state on Consul KV. We have deployed Consul Cluster in each environment as well as Consul Clients running on both VMs and as k8s DaemonSets. 

Having Consul Server cluster in k8s makes it easier to maintain and deploy the latest Consul version by using the latest Consul Helm chart, which allows us to take advantage of new features within Consul, while providing a highly available and secure Consul deployment.


## Architecture
```plantuml
@startuml

!include <logos/kubernetes>
!include <logos/consul>
!include <kubernetes/k8s-sprites-unlabeled-25pct>

!define osaPuml https://raw.githubusercontent.com/Crashedmind/PlantUML-opensecurityarchitecture2-icons/master
!include osaPuml/Common.puml
!include osaPuml/Server/all.puml

title GitLab Consul Architecture

skinparam linetype ortho
skinparam roundcorner 20

node "<$osa_server>\nVMs\n\n<$consul*0.5>\nConsul Client" as VMs

package "gprd-gitlab-gke" as regional {
  together {
    component "<$consul*0.7>\nClients Daemonset" as ClientsRegional
    component "<$consul*0.7>\nConsul DNS" as ConsulDNS
  }
  
  together {
    component "<$consul*0.7>\nServer Cluster" as ConsulCluster
    component "<$svc>\nConsul Server Service" as ConsulServerService
  }
  
  [<$kubernetes>\nk8s workloads] as K8SWorkloads
}

package "gprd-us-east1-{b,c,d}" as zonals {
  component "<$consul*0.7>\nClients Daemonset" as ClientsZonal
  component "<$consul*0.7>\nConsul DNS" as ConsulDNSZonal
  [<$kubernetes>\nk8s workloads] as K8SWorkloadsZonal
  ClientsZonal <-> ConsulDNSZonal
  K8SWorkloadsZonal -d-> ConsulDNSZonal : consul-gl-consul-dns.consul.svc.cluster.local
}

VMs <--> ConsulServerService : consul-gl-internal.ENV.gke.gitlab.net
ClientsRegional <-> ConsulDNS
ClientsRegional <-> ConsulCluster
ConsulCluster <-d-> ConsulServerService
ConsulDNS <-d- K8SWorkloads : consul-gl-consul-dns.consul.svc.cluster.local

ClientsZonal <-> ConsulServerService

@enduml
```

The Consul Cluster runs on the regional GKE cluster ($ENV-gitlab-gke) and exposes its ports using a Kubernetes Service. The cluster is composed by 5 Consul Server agents, for greater resilience and availability in the event of a failure. Consul Clients in the regional and zonal GKE clusters as well as VMs communicate with the Consul Cluster by using its exposed k8s service. Consul DNS is being exposed by a k8s Service as well and uses each local Consul Client to provide DNS resolution to the Rails workloads to be able to discover what is the Patroni primary and replica nodes.

#### Consul Components

* Consul Server Agents:
  * Consul server agents store all state information, including service and node IP addresses, health checks, and configuration. They are deployed as a StatefulSet in the regional GKE cluster.
* Consul Client Agents:
  * Consul clients report node and service health status to the Consul cluster. We also use them for Service Discovery and DNS resolution of the DB endpoints. They are deployed as DaemonSets on each GKE cluster (both regional and zonal), as well as individual client agents on VM fleets.

#### Resources and configuration

* The deployment of Consul Cluster Server and Client Agents are defined on the gitlab-helmfiles [consul release](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-helmfiles/-/tree/master/releases/consul) which uses the [Consul Helm Chart](https://github.com/hashicorp/consul-k8s/tree/main/charts/consul) and it's deployed automatically to the individual environments via Gitlab CI and Helmfile.
 
* Some infrastructure resources (IAM, GCS) are managed through Terraform via the [consul-gcs module](https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/tree/master/modules/consul-gcs) located in the [`config-mgmt`](https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/) repository.



## Backup and Restore

* A [k8s CronJob](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-helmfiles/-/blob/master/releases/consul/charts/consul-gl-extras/templates/job.yaml) creates a snapshot of the Consul Server and uploads it to GCS. The cronJob is executed every 1 hour, and we have set a retention of 365 days for the GCS bucket.
* The snapshots from GCS can be restored to a new Consul Cluster by using the [snapshot restore command](https://developer.hashicorp.com/consul/commands/snapshot/restore) as per [Consul documentation](https://developer.hashicorp.com/consul/tutorials/production-deploy/backup-and-restore).

## Monitoring and Alerts
#### Consul Metrics and Prometheus
* Consul is being monitored by Prometheus on GKE by using a [ServiceMonitor](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-helmfiles/-/blob/master/releases/consul/charts/consul-gl-extras/templates/servicemonitor.yaml) which scrapes the `/v1/agent/metrics` endpoint.

#### Logs
* Logs are being exported to Elasticsearch and can be found here:
  * [Production](https://log.gprd.gitlab.net/goto/c34c9130-7bdc-11ed-85ed-e7557b0a598c)
  * [Staging](https://nonprod-log.gitlab.net/goto/e467e8b0-7bdc-11ed-9af2-6131f0ee4ce6)

#### Dashboards, Alerts
* Consul has been added to the [metrics-catalog](https://gitlab.com/gitlab-com/runbooks/-/blob/master/metrics-catalog/services/consul.jsonnet) for defining SLI and SLOs as well as alerts for the Service.
* Grafana Dashboard:
  * [Consul Overview](https://dashboards.gitlab.net/goto/kjmfPFc4k?orgId=1)
  * [Dashboard Definition](https://gitlab.com/gitlab-com/runbooks/-/blob/master/dashboards/consul/main.dashboard.jsonnet)

#### Runbook
* The [Consul Runbook](https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/consul) has been updated to reflect the new architecture in k8s as well as instructions for the usage of the service.

## Responsibility

- Reliability Team
