[[_TOC_]]

# Migrating Web Service to Kubernetes

## Definition

For this document, the "Web Service" is comprised by all request paths by
clients that do not land on any other deployment, such as our https_git, api
or websockets backends of HAProxy.

## Overview

This work is part of the ongoing epic to migrate
[Gitlab.com to Kubernetes](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/112)

After [API](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/271), the Web
service will be the fourth rails service being moved to Kubernetes. This should
result in reduced deploy times and better resource usage while making use of the
official helm chart. Completing this work is one of the OKR's for the Delivery
team for Q3FY22: https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/112

This readiness review document is part of
https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1807

## Architecture

```mermaid
graph TB

Cloudflare

subgraph GCP
  subgraph gitlab-production
    GCP-TCP-LB
    HAPROXY

    subgraph GKE-zonal-cluster
      GCP-TCP-LB-GKE

      subgraph Namespaces

        subgraph gitlab
          subgraph web-node-pool
            subgraph Web-pods
              gitlab-workhorse
              webservice
            end
          end
        end
      end
    end
  end

  DB
  Redis
  Gitaly
end

Cloudflare -->
GCP-TCP-LB -->
HAPROXY -->
GCP-TCP-LB-GKE --> gitlab-workhorse

webservice --> DB
webservice --> Redis
webservice --> Gitaly
webservice --> Container-Registry

gitlab-workhorse --> Redis
gitlab-workhorse --> webservice

style GCP fill:#9BF;
style gitlab-production fill:#1AD;
style GKE-zonal-cluster fill:#EAD;
style web-node-pool fill:#ECD;
style Namespaces fill:#FED;
style Web-pods fill:#3E7;
style gitlab fill:#FAA;
```

## Performance

We currently (Aug 2021) serve just shy of 5k requests/s to
workhorse and puma.  Traffic behavior can be seen [on our Overview Dashboard](https://dashboards.gitlab.net/d/web-main/web-overview?orgId=1)

Performance of the Web service mainly depends on these factors:

* amount of nodes (as of writing 42 in our main stage + 9 for the canary stage)
* node type (c2-standard-16)
* number of puma worker_processes per node (16)
  * Totaling: 816 puma workers
* number of puma min (1) and max (4) threads
  * Totaling: at most 3264 threads

It is important to always have enough Web capacity so that rolling deployments,
zonal outages, or turning off canary is not affecting user experience.  Currently our VM's are
decently tuned and the only saturation is related to disk usage.

### Kubernetes

In a Kubernetes deployment we need to tune resource requests, autoscaling and
rollingUpdate settings to ensure Web is able to meet it's SLOs and can react to
surges in traffic as well as to prevent frequent Pod eviction as starting a Web
Pod is taking a long time. Web Pods take approximately 1 minutes and 10 seconds
to start up.  Note that we delay the readiness probe 60 seconds, which
contributes to the vast majority of time we are waiting.  This is done
intentionally due to issues when Pods are reporting healthy, but may still be
starting up.  This is captured in issue:
https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1294

We are focusing on the following configuration:

* Horizontal Pod Autoscaler (HPA) target values
* rollingUpdate strategy maxSurge
* minReplicas and maxReplicas
* resource requests and limits for CPU and memory

We will test those parameters by slowly shifting traffic from our VM fleet to
Kubernetes and adapting if necessary. The Web node pool is using the same
machine type as our current Web VMs. For most of the above values, we'll start
with similar values that we've come up with based on learnings from the API
migration.

* HPA Targets:
  * 3600m average Pod CPU utilization
  * 25 min Pods per cluster - totaling 75 minimum Pods during migration start
  * 150 max Pods per cluster - totaling 450 maximum Pods
  * Note the Canary stage, which runs on a single regional cluster, will use our
    webservice chart global configuration.  This matches a 3600m CPU average for
  scale, and sets the minimum Pod count to 16, and the maximum Pod count of 50
* Deployment Strategy configuration:
  * Default strategy is RollingUpdate
  * 0 max unavailable Pods
  * 25% max surge
  * Progress Deadline is 600 seconds

With maxUnavailable set to 0, we will always scale up new Pods prior to tearing
down any older Pods during a deployment or configuration change.

A few details on Pod counts; We currently run 51 Web nodes on our main and
canary stages with 16 puma workers each, for each worker we spawn between 1 and
4 threads. As we currently run 4 workers per Pod, this equates to 68 Pods per
zonal cluster.  This is well within our minReplica and maxReplica counts.  A
maximum value of 150 per cluster was chosen for two reasons.  One, if we lose a
cluster for X reason, we'll have capacity to scale upwards.  Two, for growth and
or miscalculations.  Should Kubernetes need more resources to run our Pods, or
we are growing Web traffic quickly, 150 will provide roughly 3X Pod capacity
over our initial estimates.

For the canary stage, we run 9 VM's, still with 16 workers, providing a total of
64 workers.  This fits nicely with our existing configuration.  When canary
starts taking all traffic in Kubernetes, we should see our autoscale settle with
roughly 36 Pods during peak load times.

## Migration Plan

### Current Virtual Machine configuration with HAProxy

```plantuml
skinparam roundcorner 20
skinparam shadowing false
rectangle "Frontend HAProxy" as A {
  card "web" as A1
  card "canary_web" as A2
}
rectangle "web-XX VMs" as B {
  card "canary" as B1 {
      rectangle "nginx/workhorse/puma" as B11
  }
  card "main" as B2 {
      rectangle "nginx/workhorse/puma" as B21
   }
}

A1 --> B21 : main
A2 --> B11 : cny
```

The Canary stage for Web is routed based on request path and additionally taking
a small percentage of random traffic and diverting it to the canary stage to
give it a more realistic traffic pattern.

#### Phase 1

The first phase will be to move all canary traffic in the Kubernetes cluster.
The canary VMs will be replaced with the canary namespace in the cluster. As
canary traffic is too small we will not split it into 3 zonal GKE clusters but
rather serve it all from the regional GKE cluster. This is the same method for
which our git and api canary fleet is deployed.

One important distinction is that with web running in Kubernetes, we have [made
the decision to no longer use nginx in front of workhorse](https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1924). This is because we have identified that with our current
setup for Gitlab.com, nginx provides us with no real value, and thus removing
it simplifies our stack.

```plantuml
skinparam roundcorner 20
skinparam shadowing false
rectangle "Frontend HAProxy" as A {
      rectangle "web backend" as A1
      rectangle "canary_web backend" as A2
    }
    rectangle "web-XX VMs" as B {
      card "main" as B2 {
          rectangle "nginx/workhorse/puma" as B21
       }
    }
    rectangle "Production GKE" as C {
      card "gitlab-cny namespace" as C1 {
          card "webservice pod" as C11 {
            rectangle "puma" as C111
            rectangle "workhorse" as C112
          }
       }

      card "gitlab namespace" as C2 {
          card "webservice pod" as C21 {
            rectangle "puma" as C211
            rectangle "workhorse" as C212
          }
       }
    }

    A1 --> B21
    A2 --> C112
```

* Doing so will assist us in finding any new potential blockers and validate
  that the service is working as desired.

#### Phase 2

Once we are confident in canary working appropriately, we'll begin slowly
shifting traffic from our VMs into Kubernetes. We'll do this using weights on
HAProxy. This is the same procedure we've used in the past when migrating our
git services over. After this transition is complete, the VMs can be scaled
down.

```plantuml
skinparam roundcorner 20
skinparam shadowing false
rectangle "Frontend HAProxy" as A {
      rectangle "web backend" as A1
      rectangle "canary_web backend" as A2
    }
    rectangle "web-XX VMs" as B {
      card "main" as B2 {
          rectangle "nginx/workhorse/puma" as B21
       }
    }
    rectangle "Production GKE" as C {
      card "gitlab-cny namespace" as C1 {
          card "webservice pod" as C11 {
            rectangle "puma" as C111
            rectangle "workhorse" as C112
          }
       }

      card "gitlab namespace" as C2 {
          card "webservice pod" as C21 {
            rectangle "puma" as C211
            rectangle "workhorse" as C212
          }
       }
    }

    A2 --> C112
    A1 --> C212 : 0-100%
    A1 --> B21 : 0-100%
```

### Network

The migration from Virtual Machines to GKE introduces additional network hops
due to the use of Kubernetes and removal of a local socket previously utilized
by workhorse on Virtual Machines.


#### Virtual Machine Network Topology

We currently run the `web-*` fleet on Virtual Machines that are directly
attached to HAProxy VMs. Recently, we configured HAProxy so that VMs in the same
availability zone are preferred which helped to reduce our cloud cost for
cross-zone network traffic.

```plantuml
skinparam roundcorner 20
skinparam shadowing false

cloud "Public Internet" as PI
rectangle "TCP LB :443" as TCP_LB

folder "us-east1-b" as F1B {
  rectangle "HAProxy" as HAProxyB
  storage "Web VM fleet" as WebB
}

folder "us-east1-c" as F1C  {
  rectangle "HAProxy" as HAProxyC
  storage "Web VM fleet" as WebC

}

folder "us-east1-d" as F1D {
  rectangle "HAProxy" as HAProxyD
  storage "Web VM fleet" as WebD

}

PI -- TCP_LB
TCP_LB -- HAProxyB
TCP_LB -- HAProxyC
TCP_LB -- HAProxyD
HAProxyB -- WebB
HAProxyC -- WebC
HAProxyD -- WebD
```

Notes:

* Lines that cross availability zones are where we are billed at a higher rate
* In our current configuration there are a few points of cross-zone network
  traffic, it occurs between the `web-*` fleet and our stateful backends,
  Gitaly, Postgres, and Redis.

### GKE Network Topology for Webservice

```plantuml
skinparam roundcorner 20
skinparam shadowing false

cloud "Public Internet" as PI


rectangle "GitLab.com TCP LB :443" as TCP_LB

PI -- TCP_LB

folder "us-east1-b" as F1B {
  rectangle "HAProxy" as HAProxyB
  rectangle "Internal GKE TCP LB :8181" as GKE_LBB
  node "webservice pod" as WebB
}

folder "us-east1-c" as F1C  {
  rectangle "HAProxy" as HAProxyC
  rectangle "Internal GKE TCP LB :8181" as GKE_LBC
  node "webservice pod" as WebC
}


folder "us-east1-d" as F1D {
  rectangle "HAProxy" as HAProxyD
  rectangle "Internal GKE TCP LB :8181" as GKE_LBD
  node "webservice pod" as WebD
}

TCP_LB -- HAProxyB
TCP_LB -- HAProxyC
TCP_LB -- HAProxyD


HAProxyB -- GKE_LBB
HAProxyC -- GKE_LBC
HAProxyD -- GKE_LBD

GKE_LBB -- WebB
GKE_LBC -- WebC
GKE_LBD -- WebD
```

Notes:

* In our Kubernetes infrastructure, the connection between workhorse and puma
  changes from a local unix socket `
  unix:/var/opt/gitlab/gitlab-rails/sockets/gitlab.socket` to a TCP connection
* We are removing NGINX from between HAProxy and the Web deployment.

## Operational Risk Assessment

## What are the internal and external dependencies of this service, are there SPOFs?

### Consul

Consul and Consul-DNS is used for database [service
discovery](https://docs.gitlab.com/ee/administration/database_load_balancing.html#service-discovery)
to fetch the list of replicas.  In the situation where Consul is degraded the
application will gracefully revert to only using the Primary database.

For example, if Consul is failing to return results, all rails applications will
result in these application errors:

```
Service discovery encountered an error: No response from nameservers list
Sending event 1b60bcdb190144479175dda4a79bdd30 to Sentry
```

But the application will still function, though more pressure will be put on the primary DB.

In order to reduce cross node traffic and alleviate issues where connectivity
to consul was failing due to node scale down events (as documented
[here](https://gitlab.com/gitlab-org/gitlab/-/issues/271575)) we configure
our web pods to only talk to the consul instance running on the same node, as
seen [here](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/blob/master/releases/gitlab/values/values.yaml.gotmpl#L588-602) and
[here](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/blob/master/releases/gitlab/values/gprd.yaml.gotmpl#L453-461)

### Existing dependencies outside of the cluster

The following dependencies are dependencies of the Web service, but are already
present for Sidekiq, git-https, API, and websockets.

* Consul
* Gitaly
* HAProxy
* Patroni / PgBouncer
* Redis (persistent/cache/sidekiq)

## What are the potential scalability or performance issues that may result with this change?

### Scaling

There is a risk that we will not be able to adapt quickly enough to increases in
traffic, or sudden increases will cause saturation on the node. To mitigate this
we have:

* Isolating this workload in its own node-pool will minimize impact to other
  services.
* The ability to tweak the HPA configuration
* Tweaking the node pool instance types that this workload runs is also an option
* During the migration, we'll be carefully monitoring the metrics associated
  with this service and adjust our scaling configuration as necessary until we
  are taking 100% of the traffic in Kubernetes.
* Should we conveniently suffer a DDOS attack of sorts, we already have a couple
  of helpful items:
  * CloudFlare WAF
  * RackAttack
  * HAProxy Rate Limiting ACL rules
  * Along with the above, if we determine that an attack is negatively impacting
    us, this will undoubtedly be a high severity incident.  In this case, we'll
    want to ensure the stability of the system comes first.  This may mean
    immediately halting the transition to a well known, good state during
    mitigation.

### Log volume

We don't expect any performance issues due to logging, but will be monitoring
this closely during the migration. We will see an increase in the data being
captured that differs from our normal traffic patterns.  This includes the
following items:

* an increase in logging data for the `/-/readiness` check log messages, due to
  the reconfiguration of HAProxy and readiness requests in Kubernetes.
* The start and stopping of Pods will create a larger influx of messages related
  to those services' start up and shutdown events.  Currently we only see this
  from our VM's during mostly deployments.  However, we'll be rotating through
  more Pods during a deployment along with extra events from Pods that scale with
  our Auto-scaling mechanism
* Additional logs that weren't previously being captured from VM's.  The volume
  of this extra log data is relatively low, but may serve as useful and so we
  will keep these when transitioning into Kubernetes.  These extra logs include
  data from our database_load_balancing log, web_exporter, sidekiq_client,
  graphql, elasticsearch, audit, auth.  Feel free to surf these logs in our non
  prod environment to get a feel into what data is included in these logs.

### Prometheus metrics

Prometheus metrics are how we observe the behavior of our entire stack.  The
additional Pods will greatly increase the amount of metrics our systems will
need to process.  We will be monitoring Thanos, specifically the thanos-rule
process to ensure that we do not overwhelm our rule aggregations.  We monitor
this service heavily with its own set of SLO alerts.  Should any alerts trip
when we begin running this service in production, we'll need to stop and
evaluate how we can ease the pressure on our monitoring infrastructure.

## List the top operational risks when this feature goes live.

* Unexpected configuration differences between Cloud Native and Omnibus. We try
  to reduce the risk by auditing configuration in all environments.
  https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1065
  * The same audit has occurred when we first deployed the service into canary.
* We'll be switching from the NGINX service that runs on our VM's to sending
  traffic directly from HAProxy to Workhorse.
  * Details on how we landed with this configuration are in [Issue 1924](https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1924)
  * We currently think that both CloudFlare and HAProxy provide us with enough
    protection from client abuse situations.
  * We've removed NGINX in our git service migrations as NGINX wasn't doing any
    heavy lifting.
  * This means that we are removing encryption of traffic between our HAProxy
    layer and that of our webservice.
* Issues with HPA, not enough reserved capacity and saturation problems related
  to scaling
  * This is captured as a Saturation Alert that pages the EOC.
* Disruption and user impact during upgrades due to long-lived connections
  * This has been evaluated and adjustments to our deployment configuration have
    been made to shorten deployments without any observed user impact.  See the
  results of this testing here:
  https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1558
* Issues with metrics overwhelming the current in-cluster Prometheus and
  our Thanos rule aggregation systems.
* PGBouncer Saturation: We'll be monitoring pgbouncer closely to ensure that we
  are not unnecessarily saturating this service when making calls to our
  database.  Should we see connection saturation, this may delay the rollout of
  this service.  Saturation may come in the form of connected clients as the
  amount of running Pods will be far higher than the number of VM's.

## Security and Compliance

The security stature of this service does not change.  The scope of managing
security, however, will change as this is a shift in where the service is
running (Virtual Machines vs Kubernetes).  This service will receive security
updates in accordance to the [GitLab Release and Maintenance
Policy](https://docs.gitlab.com/ee/policy/maintenance.html)

### Configurations

Infrastructure changes and configurations are not lightweight and must go
through a rigorous review procedure and be vetted and tested prior to being
brought into our production environments.  Configurations that involve changes
that would modify our exposure or security posture, should also be reviewed by
our security team to ensure any proposal is appropriately vetted for any
potential risk.  We have the procedure configuration change documented in the
repository's [DEPLOYMENT.md](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/blob/master/DEPLOYMENT.md#configuration-changes) document.

If a proposed change contains unknown security concerns, there should be no
hesitation to rope in the security team to provide the needed confidence prior
to allowing such a change into our environments.


More details on reaching out to the appropriate team members can be found here:
https://about.gitlab.com/handbook/engineering/security/security-engineering-and-research/application-security/

### Application Upgrade and Rollback

The deployment mechanism for the Web will mirror that of all other services
which utilize our own helm chart.  Please see our [existing Kubernetes
deployment documentation.](https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/deploy/gitlab-com-deployer.md#kubernetes-upgrade)

The rails application uses the default BLACKOUT Period of 10 seconds.  Upon the
application receiving the first `SIGTERM`, the readiness probe will begin to
fail which pulls the Pod from receiving any future traffic.  We configure the
Pod terminationGracePeriodSeconds to 65.  This will allow any remaining requests
that are being serviced to complete prior to the Pod being forcibly removed from
the Infrastructure.  We are using similar values to that of our current API
configuration.  See issue
https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1558 where we tuned
these values during testing.

## Performance

### Resource Configuration

For the webservice container which runs Puma:

```yaml
resources:
  limits:
    memory: 6G
  requests:
    cpu: 4 (that's 4 total cpu cores, or 4000m)
    memory: 5G
```

For the workhorse container:

```yaml
resources:
  limits:
    memory: 2G
  requests:
    cpu: 600m (.6 cores)
    memory: 200M
```

This should enable 3 pods fit on a node (15.89 allocatable CPUs, 61.5G
allocatable memory).  This equates to roughly 23 nodes allocated to run this
workload.  We set our node pools with a minimum of 10 nodes per zone with a
maximum of 50.  So we'll have plenty of capacity to play with for our node
configuration and this will allow greater tuning for more efficiency in the
future.

## Backup and Restore

This is a stateless component of GitLab.  No work to be done regarding backups
and restoration of customer data with this migration.

## Monitoring and Alerts

### Logging

Logging remains the same before and after this migration. One can view [our
runbook for Logging](https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/logging#logging-infrastructure-overview) which contains a lot of details as to how logging works
for this infrastructure.

We will mostly be using the same log indexes when running in Kubernetes, with
some small changes:

* All logs from the webservice container will be forwarded to the rails index.
  This includes both puma and rails, and all unstructured logs present in
  `/var/log/gitlab/*`.
* All logs from the workhorse container will be forwarded to the workhorse index
* There are additional logs that will be forwarded to the ElasticSearch cluster
  that we are currently ignoring on VMs. The volume of these extra logs is low
  and therefore do not believe this will cause too much additional load on the
  cluster, and we have [disabled the unstructured production.log and
  application.log](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/merge_requests/347).
* Logging coming from the puma Pod is serviced by
  [GitLab-Logger](https://gitlab.com/gitlab-org/charts/components/gitlab-logger)

### Monitoring

We currently have a wide scope of monitoring that comes from the use of
`kube-state-metrics` as well as the exporter that is built into workhorse and
puma applications.  We'll continue to leverage our existing alerting rules to
warn us of problems related to the operational state of these Pods from the
scope of running inside of Kubernetes.

We also have a suite of metrics specifically monitoring the Web backend in
HAProxy which will alert us if we see excessive error rates or a drop in Apdex.

## Responsibility

SME's:

* Infrastructure:
  * Delivery
  * Scalability
  * Reliability
* Deployment: ~team::Delivery
* Helm Chart: ~team::Distribution
* Web:
  * ~group::source code

## Testing

No testing was performed as this is a service that already exists in our
environment.  All testing has been circled around the migration of this service
into Kubernetes.  Refer to the associated epic for testing that was performed.
https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/272

We leveraged knowledge of recent migrations, API, https_git to assist us with
helping guide decision making for this migration.

Some highlights:

* Configuration auditing:
  https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1065
* Traffic behavior during deployments or Pod outages:
  https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1558

## Known issues going into production

With the removal of NGINX, the migration of the service is made slightly
difficult.  This is being refined both in this document as well as [deployment
issue.](https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1894)

Our Web VM's occaisionally suffer from disk saturation as a result of a specific
file upload that sometimes fails to clean up after itself.  We've temporarily
remediated this by mounting an `emptyDir` configuration to prevent Web Pods from
building up excessive temporary files.  A long term fix would allow us to remove
this configuration.  References:

* [Example Incident](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/5042)
* [Remediation prior to migration](https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues/1864)
* [Long Term Fix](https://gitlab.com/groups/gitlab-org/-/epics/6396)

## Readiness Reviewers

### Engineering

* @stanhu

### Infrastructure

* @pguinoiseau
* @cmcfarland 

### Security

* @mlancini
